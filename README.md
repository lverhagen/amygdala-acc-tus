# amygdala-ACC-TUS
This is an open-source repository with the data supporting the publication:

### Manipulation of subcortical and deep cortical activity in the primate brain using transcranial focused ultrasound stimulation  

#### Davide Folloni<sup>1,2</sup>\*, Lennart Verhagen<sup>1,2</sup>\*, Rogier B. Mars<sup>2,3</sup>, Elsa Fouragnan<sup>1,4</sup>, Charlotte Constans<sup>5</sup>, Jean-François Aubry<sup>5</sup>, Matthew F.S. Rushworth<sup>1,2</sup>†, Jérôme Sallet<sup>1,2</sup>†  

<sup>1</sup> Wellcome Centre for Integrative Neuroimaging (WIN), Department of Experimental Psychology, University of Oxford, Oxford OX1 3SR, UK  
<sup>2</sup> Wellcome Centre for Integrative Neuroimaging (WIN), Centre for Functional MRI of the Brain (FMRIB), Nuffield Department of Clinical Neurosciences, John Radcliffe Hospital, University of Oxford, Oxford OX3 9DU, UK  
<sup>3</sup> Donders Institute for Brain, Cognition and Behaviour, Radboud University Nijmegen, 6525 HR Nijmegen, the Netherlands  
<sup>4</sup> School of Psychology, University of Plymouth, Plymouth PL4 8AA, UK  
<sup>5</sup> Institut Langevin, ESPCI Paris, PSL Research University, CNRS 7587, UMRS 979 INSERM, Université Paris Diderot, Sorbonne Paris Cité, Paris, France  
\* These authors contributed equally  
† These authors contributed equally  

Correspondence:  
davide.folloni@psy.ox.ac.uk  
lennart.verhagen@psy.ox.ac.uk  
jerome.sallet@psy.ox.ac.uk  

# data organisation
MRI data are organised in the folder **_data_** according to condition > monkey > data-type
- control
  - MK01
    - rfMRI
    - T1w
  - MK02, etc
- amygdalaTUS
  - MK03
    - rfMRI
    - T1w
  - MK04, etc
- accTUS
  - MK05
    - rfMRI
    - T1w
  - MK06, etc

# additional information
documents with additional information are stored in the folder **_info_**
- anesthesia_parameters.csv  
  *anesthesia induction times and physiological measurements for each session*
